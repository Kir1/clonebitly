export const INI = 'INI';
export const CREATE_LINK = 'CREATE_LINK';
export const REMOVE_LINK = 'REMOVE_LINK';
export const GO_TO_LINK = 'GO_TO_LINK';

export const initialApp = () => ({
    type: INI
})

export const createLink = (link: string) => ({
    type: CREATE_LINK,
    link
})

export const removeLink = (id: number) => ({
    type: REMOVE_LINK,
    id
})

export const goToLink = (id: number) => ({
    type: GO_TO_LINK,
    id
})