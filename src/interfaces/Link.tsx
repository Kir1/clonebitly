export interface LinkInterface {
  id: number,
  originalLink: string,
  shortLink: string,
  counter: number,
}