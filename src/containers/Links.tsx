import Link from "../components/Link";
import Grid from "@material-ui/core/Grid";
import {removeLink, goToLink} from "../actions/links";
import {connect} from 'react-redux';
import React from 'react';

function Links(props) {
    const links = props.links.map((link, index) =>
        <Link key={index} id={link.id} originalLink={link.originalLink} counter={link.counter}
              shortLink={link.shortLink}
              goToLink={(event: any) => {
                  event.preventDefault();
                  props.goToLink(link.id);
              }}
              remove={() => {
                  props.removeLink(link.id);
              }}
        />
    );

    return (
        <Grid container direction="column" justify="center" alignItems="center">
            {links.length > 0 ? links : 'Пока ссылок нет'}
        </Grid>
    );
}

const mapStateToProps = (state, ownProps) => ({links: state.links});

const mapDispatchToProps = (dispatch, ownProps) => ({
    goToLink: (id) => dispatch(goToLink(id)),
    removeLink: (id) => dispatch(removeLink(id)),
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Links)