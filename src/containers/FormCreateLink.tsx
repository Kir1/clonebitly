import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import {createStyles, makeStyles, Theme} from "@material-ui/core/styles";
import {createLink} from "../actions/links";
import {connect} from 'react-redux';
import Alert from '@material-ui/lab/Alert';
import {Link} from "react-router-dom";
import React from 'react';
import {validURL} from "../helpers/link";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        formElement: {
            paddingBottom: theme.spacing(3),
            minWidth: '300px',
            maxWidth: '500px',
            display: 'block'
        },
        alert: {
            marginTop: '10px'
        }
    }),
);

function FormCreateLink(props) {
    const [state, setState] = React.useState({
        link: '',
        cratedLink: ''
    });

    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setState({...state, link: event.target.value});
    };

    const createLink = (link: string) => {
        if (validURL(link)) {
            setState({...state, link: '', cratedLink: link});
            props.createLink(state.link);
        }
    };

    const classes = useStyles();

    return (
        <Grid container spacing={4} direction="column" justify="center" alignItems="center">
            <TextField
                fullWidth
                id="standard-helperText"
                label="Впишите ссылку"
                className={classes.formElement}
                value={state.link}
                onChange={handleChange}
            />
            <Button variant="contained" color="primary" onClick={(e) => createLink(state.link)}>
                Получить краткую ссылку
            </Button>
            {state.cratedLink &&
            <Alert severity="success"  className={classes.alert}>
                Ссылка {state.cratedLink}
                <div>
                    создана. Можете увидеть её вначале <Link to='/'>списка</Link>
                </div>
            </Alert>
            }
        </Grid>
    );
}

const mapStateToProps = (state, ownProps) => ({});

const mapDispatchToProps = (dispatch, ownProps) => ({
    createLink: (link) => dispatch(createLink(link))
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(FormCreateLink)
