import LinksRouter from "./pages/links/LinksRouter";
import LinksPage from "./pages/links/LinksPage";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";
import Grid from '@material-ui/core/Grid';
import {makeStyles, createStyles, Theme} from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Hidden from '@material-ui/core/Hidden';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import './App.scss';
import {initialApp} from "./actions/links";
import {connect} from 'react-redux';
import React, {useEffect} from 'react';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1
        },
        container: {
            padding: theme.spacing(3)
        },
        menuButton: {
            marginRight: theme.spacing(2),
        },
        link: {
            color: 'white',
            marginRight: theme.spacing(2),
            textDecoration: 'none'
        },
        mobileMenuLink: {
            color: 'black',
            marginRight: theme.spacing(2),
            textDecoration: 'none'
        },
        list: {
            width: 250,
        },
        fullList: {
            width: 'auto',
        }
    }),
);

function App(props) {
    const classes = useStyles();

    const [state, setState] = React.useState({
        showMobileMenu: false
    });

    const toggleDrawer = (open: boolean) => (
        event: React.KeyboardEvent | React.MouseEvent,
    ) => {
        if (
            event.type === 'keydown' &&
            ((event as React.KeyboardEvent).key === 'Tab' ||
                (event as React.KeyboardEvent).key === 'Shift')
        ) {
            return;
        }
        setState({...state, showMobileMenu: open});
    };

    const goToLink = () => setState({...state, showMobileMenu: false});

    const links = [
        {to: '/', text: 'Главная'},
        {to: '/links/create', text: 'Создать ссылку'},
        {to: '/links', text: 'Список ссылок'},
    ];
    const linksPC = links.map((link, index) =>
        <Link key={index} className={classes.link} onClick={goToLink} to={link.to}>{link.text}</Link>);
    const linksMobile = links.map((link, index) =>
        <Link key={index} className={classes.mobileMenuLink} onClick={goToLink} to={link.to}>{link.text}</Link>);


    useEffect(() => {
        props.ini()
    });

    return (
        <div className={classes.root}>
            <Grid container>
                <Router>
                    <AppBar position="static">
                        <Toolbar>
                            <Hidden mdUp>
                                <IconButton edge="start" className={classes.menuButton}
                                            color="inherit"
                                            aria-label="menu"
                                            onClick={toggleDrawer(true)}
                                >
                                    <MenuIcon/>
                                </IconButton>
                                <Drawer anchor='left' open={state['showMobileMenu']} onClose={toggleDrawer(false)}>
                                    <List>
                                        {linksMobile.map((link, index) => (
                                            <ListItem button key={index}>
                                                {link}
                                            </ListItem>
                                        ))}
                                    </List>
                                </Drawer>
                            </Hidden>
                            <Hidden smDown>
                                {linksPC}
                            </Hidden>
                        </Toolbar>
                    </AppBar>
                    <Grid item container className={classes.container} xs={12} direction="row" justify="center">
                        <Switch>
                            <Route exact path="/" component={LinksPage}/>
                            <Route path='/links' component={LinksRouter}/>
                        </Switch>
                    </Grid>
                </Router>
            </Grid>
        </div>
    );
}

const mapStateToProps = (state, ownProps) => ({});

const mapDispatchToProps = (dispatch, ownProps) => ({
    ini: () => dispatch(initialApp())
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(App)
