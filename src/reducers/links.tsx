import {GO_TO_LINK, REMOVE_LINK, CREATE_LINK, INI} from "../actions/links";
import {LinkInterface} from "../interfaces/Link";
import {generateShortLink, validURL} from "../helpers/link";

function chaneLinkAndSaveStorage(links, id, action = (linkIndex) => {
}) {
    let linkIndex = links.findIndex(link => link.id === id);
    if (linkIndex !== -1 && action instanceof Function) {
        action(linkIndex);
        localStorage.setItem('links', JSON.stringify(links));
    }
    return links;
}

export interface StateLinksInterface {
    links: LinkInterface[]
}

const initialState = {
    links: []
}

export default (state: StateLinksInterface = initialState, action) => {
    switch (action.type) {
        case INI: {
            let links: LinkInterface[] = [], linksStorage: any = localStorage.getItem('links');
            if (linksStorage) links = JSON.parse(linksStorage); else localStorage.setItem('links', '');
            return {...state, links};
        }

        case GO_TO_LINK: {
            let links: LinkInterface[] = [...state.links];
            links = chaneLinkAndSaveStorage(links, action.id, (linkIndex: number) => {
                links[linkIndex] = {...links[linkIndex]};
                links[linkIndex].counter++;
                window.location.href = links[linkIndex].originalLink;
            });
            return {...state, links};
        }

        case REMOVE_LINK: {
            let links: LinkInterface[] = [...state.links];
            links = chaneLinkAndSaveStorage(links, action.id, (linkIndex: number) => {
                links.splice(linkIndex, 1);
            });
            return {...state, links};
        }

        case CREATE_LINK: {
            let links: LinkInterface[] = [...state.links];
            if (validURL(action.link)) {
                let id = links.length + 1;
                links.unshift(
                    {
                        id,
                        originalLink: action.link,
                        shortLink: generateShortLink(id),
                        counter: 0
                    }
                );
                localStorage.setItem('links', JSON.stringify(links));
            }
            return {...state, links};
        }

        default:
            return state
    }
}