import DeleteOutlinedIcon from '@material-ui/icons/DeleteOutlined';
import Grid from "@material-ui/core/Grid";
import Tooltip from '@material-ui/core/Tooltip';
import {LinkInterface} from '../interfaces/Link';
import {Link as LinkMaterial} from '@material-ui/core';
import {createStyles, makeStyles, Theme} from "@material-ui/core/styles";
import React from 'react';

export interface ComponentLinkInterface extends LinkInterface {
    goToLink(event): void,
    remove(): void,
}

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        container: {
            boxShadow: '0 0 10px rgba(0,0,0,0.5)',
            maxWidth: '500px',
            margin: '10px',
            padding: '10px'
        },
        child: {
            padding: '10px',
        },
        button: {
            cursor: 'pointer',
            padding: '10px'
        }
    }),
);

export default function Link(link: ComponentLinkInterface) {
    const classes = useStyles();
    return (
        <Grid container item className={classes.container} direction="row" justify="center"
              alignItems="center">
            <Grid item className={classes.child}>
                <Tooltip title={link.originalLink} placement="top-start">
                    <LinkMaterial href={link.originalLink} onClick={link.goToLink}>
                        {link.shortLink}
                    </LinkMaterial>
                </Tooltip>
            </Grid>
            <Grid item className={classes.child}>
                Количество переходов: {link.counter}
            </Grid>
            <Grid item className={classes.button}>
                <DeleteOutlinedIcon onClick={link.remove}/>
            </Grid>
        </Grid>
    );
}