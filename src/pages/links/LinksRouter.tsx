import CreateLinkPage from "./CreateLinkPage";
import LinksPage from "./LinksPage";
import {
    useRouteMatch
} from "react-router-dom";
import {Switch, Route} from "react-router-dom";
import React from 'react';

export default function LinksRouter() {
    let {url} = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${url}`} component={LinksPage}/>
            <Route path={`${url}/create`} component={CreateLinkPage}/>
        </Switch>
    );
}