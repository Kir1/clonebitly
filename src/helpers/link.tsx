export function validURL(str) {
    let pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
        '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
        '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
        '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
        '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
        '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
    let valid = !!pattern.test(str);
    if (valid === false) alert('Ссылка невалидна');
    return valid;
}

export function generateShortLink(numb: number) {
    let link = '';

    //для локалки
    if (window.location.hostname.substring(0,3) !== 'http') link += 'http://';
    ///

    link += window.location.hostname + "/" + numb.toString() + Math.random().toString(36).substring(2, 5) + Math.random().toString(36).substring(2, 5);

    return link;
}